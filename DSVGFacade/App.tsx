import React, { Component } from 'react';
import { View, StyleSheet, Text } from 'react-native';
import Login from "./components/Login";
import ErrorBoundary from "./components/ErrorBoundary";

type AppProps = {};
type AppState = { logged_in };
export default class App extends Component<AppProps, AppState> {
    constructor(props) {
        super(props)

        this.state = {
            logged_in: false,
        };
    }

    onLoginSuccess() {
        this.setState({logged_in: true});
    }

    render() {
        const { logged_in } = this.state;

        if (!logged_in)
            return (
                <ErrorBoundary>
                    <Login onLogin={this.onLoginSuccess.bind(this)}/>
                </ErrorBoundary>
            );

        if (logged_in)
            return (
                <View style={styles.container}>
                    <Text>Logged In</Text>
                </View>
            );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: 'column',
        alignItems: 'stretch',
        justifyContent: 'center',
        backgroundColor: '#ecf0f1',
    },
    input: {
        height: 100,
        padding: 10,
        borderWidth: 1,
        borderColor: 'black',
        marginBottom: 10,
    },
});
