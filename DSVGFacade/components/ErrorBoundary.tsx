import { Component } from 'react';

type ErrorBoundaryState = { hasError };
type ErrorBoundaryProps = { children };
class ErrorBoundary extends Component<ErrorBoundaryProps, ErrorBoundaryState> {
    constructor(props) {
        super(props);
        this.state = { hasError: false };
    }

    static getDerivedStateFromError(error) {
        // Update state so the next render will show the fallback UI.
        return { hasError: true };
    }

    componentDidCatch(error, errorInfo) {
        // You can also log the error to an error reporting service
        // TODO: Log errors to AWS
        // logErrorToMyService(error, errorInfo);
    }

    render() {
        if (this.state.hasError) {
            return <h1>Something went wrong, the developer has been alerted and a error log has been submited.</h1>;
        }
        return this.props.children; 
    }
}

export default ErrorBoundary;
