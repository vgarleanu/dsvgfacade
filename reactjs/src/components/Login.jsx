import React, { Component } from "react";
import { Link } from "react-router-dom";

import Input from "./helpers/Input.jsx";

import "./Login.scss";

class Login extends Component {
	render() {
		return (
			<div className="login">
				<h2>Login</h2>
				<label>
					<p>Site ID</p>
					<Input id="id" type="text" placeholder="My ID is..."/>
				</label>
				<label>
					<p>Password</p>
					<Input id="password" type="password" placeholder="My password is..."/>
				</label>
                <Link to="/">
                    <button>Login</button>
                </Link>
			</div>
		);
	}
}

export default Login;
