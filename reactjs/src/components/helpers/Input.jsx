import React, { Component } from "react";

class Input extends Component {
    render() {
        return (
            <input
                {...this.props}
                autoComplete="off"
                autoCorrect="off"
                autoCapitalize="off"
                spellCheck="false"
            />
        )
    }
}

export default Input;