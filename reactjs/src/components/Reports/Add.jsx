import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Modal from "react-modal";

import Input from "../helpers/Input.jsx";

import "./Add.scss";

class Add extends Component {
	constructor(props) {
		super(props);

		this.onChange = this.onChange.bind(this);

        this.state = {
			visible: false
        };
	}

	onChange(e) {
		const {id, value} = e.target;

		this.setState({
			report: {
				...this.state.report,
				[id]: value
			}
		});
	}

    open = () => {
		this.setState({
			visible: true,
			report: {}
		});
	};

	close = () => this.setState({visible: false});

    add = () => {
		const report = {
			name: this.state.report.name,
			desc: this.state.report.comments,
			date: new Date()
		};

        this.props.addReport(report);
        this.close();
	}

	render() {
		return (
            <Fragment>
				<div className="button-wrapper">
					<button className="add" onClick={this.open}>New Report</button>
				</div>
				<Modal
                    isOpen={this.state.visible}
                    contentLabel="addReport"
                    className="addReport"
                    onRequestClose={this.close}
                    overlayClassName="popupOverlay"
                >
					<h2>Add report</h2>
					<label>
						<p>Report Name</p>
						<Input id="name" type="text" onChange={this.onChange} placeholder="The task is..."/>
					</label>
					<label>
						<p>Attach photos</p>
						<Input type="file" id="image" accept="image/*"/>
					</label>
					<label>
						<p>Add workers</p>
						<select id="workers">
							<option value="workerName">Worker 1</option>
							<option value="workerName">Worker 2</option>
							<option value="workerName">Worker 3</option>
							<option value="workerName">Worker 4</option>
						</select>
					</label>
					<label>
						<p>Comments</p>
						<textarea id="comments" rows="5" onChange={this.onChange} placeholder="Comment on the task..."/>
					</label>
					<label>
						<p>Location</p>
						<Input id="location" type="text" placeholder="The location is at..."/>
					</label>
					<Link onClick={this.close} to="/">
						<button>Cancel</button>
					</Link>
					<Link onClick={this.add} to="/">
						<button>Add Report</button>
					</Link>
				</Modal>
			</Fragment>
		);
	}
}

export default Add;
