import React, { Component } from "react";

import Edit from "./Edit.jsx";
import "./Report.scss";

const options = {
	day: "2-digit", month: "2-digit", year: "numeric",
	hour: "2-digit", minute: "2-digit", second: "2-digit"
};

class Card extends Component {
	render() {
		const {
			name,
			desc,
			date,
			id
		} = this.props.data;

		return (
			<div className="card">
				<header>
					<h3>{name}</h3>
					<p className="date">{date?.toLocaleDateString("en-US", options)}</p>
				</header>
				<p className="desc">{desc}</p>
				<div className="buttons">
					<Edit edit={(report) => this.props.editReport(id, report)} data={this.props.data}/>
					<button onClick={() => this.props.deleteReport(id)}>Delete</button>
				</div>
			</div>
		);
	}
}

export default Card;
