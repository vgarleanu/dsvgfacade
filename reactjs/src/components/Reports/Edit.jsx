import React, { Component, Fragment } from "react";
import { Link } from "react-router-dom";
import Modal from "react-modal";

import Input from "../helpers/Input.jsx";

import "./Edit.scss";

class Edit extends Component {
	constructor(props) {
		super(props);

		this.onChange = this.onChange.bind(this);

        this.state = {
            visible: false
        };
    }

	onChange(e) {
		const {id, value} = e.target;
		this.setState({[id]: value});
	}

    open = () => {
		this.setState({
            visible: true,
            name: this.props.data.name,
            comments: this.props.data.desc
        });
    };

    close = () => this.setState({visible: false});

    edit = () => {
		const report = {
			name: this.state.name,
			desc: this.state.comments
        };

        this.props.edit(report);
        this.close();
    }

	render() {
		return (
            <Fragment>
				<div className="button-wrapper">
					<button className="edit" onClick={this.open}>Edit</button>
				</div>
				<Modal
                    isOpen={this.state.visible}
                    contentLabel="editReport"
                    className="editReport"
                    onRequestClose={this.close}
                    overlayClassName="popupOverlay"
                >
					<h2>Edit report</h2>
					<label>
						<p>Report Name</p>
						<Input id="name" type="text" onChange={this.onChange} value={this.state.name}/>
					</label>
					<label>
						<p>Comments</p>
						<textarea id="comments" rows="5" onChange={this.onChange} value={this.state.comments}/>
					</label>
					<Link onClick={this.close} to="/">
						<button>Cancel</button>
					</Link>
					<Link onClick={this.edit} to="/">
						<button>Edit Report</button>
					</Link>
				</Modal>
			</Fragment>
		);
	}
}

export default Edit;
