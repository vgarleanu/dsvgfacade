import React, { Component } from "react";
import { Link } from "react-router-dom";
import Modal from "react-modal";

import SignOffModal from "./SignOffModal.jsx";
import Add from "./Add.jsx";
import Report from "./Report.jsx";

import "./List.scss";

Modal.setAppElement("body");

class Reports extends Component {
	constructor(props) {
		super(props);

		this.addReport = this.addReport.bind(this);
		this.deleteReport = this.deleteReport.bind(this);
		this.editReport = this.editReport.bind(this);

		this.state = {
			reports: []
		};
	}

	signOff = () => this.setState({reports: []});

	addReport(report) {
		const newReports = [report, ...this.state.reports];

		this.setState({
			reports: newReports.map((card, i) => ({...card, id: i}))
		});
	};

	deleteReport(id) {
		const newReports = this.state.reports.filter(card => card.id !== id);

		this.setState({
			reports: newReports.map((card, i) => ({...card, id: i}))
		});
	};

	editReport(id, newCard) {
		const newReports = this.state.reports.map(card => {
			if (card.id === id) {
				return {...newCard, id};
			}

			return card;
		});

		this.setState({
			reports: newReports
		});
	};

	render() {
		const reports = this.state.reports.map((card, i) => {
			return <Report key={i} data={{...card, id: i}} deleteReport={this.deleteReport} editReport={this.editReport}/>;
		});

		return (
			<div className="reports">
				<h2>Reports</h2>
				<div className="reports-list">
					{reports.length === 0 ? "No reports" : reports}
				</div>
				<div className="options">
					<Link className="logout" to="/login">Logout</Link>
					<SignOffModal className="signoff" continue={this.signOff}/>
					<Add className="newreport" addReport={this.addReport}/>
				</div>
			</div>
		);
	}
}

export default Reports;
