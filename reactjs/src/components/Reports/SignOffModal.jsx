import React, { Component, Fragment } from "react";
import Modal from "react-modal";

import "./SignOffModal.scss";

class SignOffModal extends Component {
    constructor(props) {
        super(props);

        this.state = {
            visible: false
        };
    }

    open = () => this.setState({visible: true});
    close = () => this.setState({visible: false});

    confirm = () => {
        this.props.continue();
        this.close();
    }

    render() {
        return (
            <Fragment>
                <div className="button-wrapper">
					<button onClick={this.open}>Sign Off</button>
				</div>
                <Modal
                    isOpen={this.state.visible}
                    contentLabel="signOffConfirm"
                    className="signOffConfirm"
                    onRequestClose={this.close}
                    overlayClassName="popupOverlay"
                >
                    <h3>CONFIRM SIGN OFF</h3>
                    <p>Are you sure you want to sign off?</p>
                    <label>
                        <p>Signature</p>
                        <input id="signature" type="text" placeholder="Sign here"/>
                    </label>
                    <div className="options">
                        <button className="cancel" onClick={this.close}>CANCEL</button>
                        <button className="continue" onClick={this.confirm}>SIGN OFF</button>
                    </div>
                </Modal>
            </Fragment>
        )
    }
}

export default SignOffModal;
