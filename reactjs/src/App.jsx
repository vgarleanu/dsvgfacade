import React, { Component } from "react";
import { Route, BrowserRouter, Switch } from "react-router-dom";

import './App.scss';

import listReports from "./components/reports/List.jsx";
import Login from "./components/Login.jsx";

class App extends Component {
	render() {
		return (
			<BrowserRouter>
			<Switch>
				<Route exact path="/" component={listReports}/>
				<Route exact path="/login" component={Login}/>
			</Switch>
			</BrowserRouter>
		);
	}
}

export default App;
